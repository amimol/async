const delay = (delay, resolveVal) => new Promise(resolve => {
  setTimeout(() => {
    resolve(resolveVal)
  }, delay)
});

const promiseA = delay(1000, 1);
const promiseB = delay(500, 2);


const execute = async () => {
  const result1 = await promiseA;
  const result2 = await promiseB;

  console.log(result1, result2);
};

execute();
