const fs = require('mz/fs');
const path = require('path');

const filePath = path.resolve(__dirname, 'bar.txt');
/*
(async () => {
  const fileData  = await fs.readFile(filePath);

  console.log(fileData.toString());
})();
*/

/*
(async () => {
  try {
    const fileData  = await fs.readFile('foo');
    console.log(fileData.toString());
  } catch (err) {
    console.log(`Error message: ${err}`);
  }
})();
*/
