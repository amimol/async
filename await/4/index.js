const fs = require('mz/fs');
const path = require('path');
const glob = require('glob-promise');

const dirPath = path.resolve(__dirname, 'data');

//
// const getFilesData = async(dataSetDir) => {
//   const elements = await fs.readdir(dataSetDir);
//   const result = [];
//
//   elements.forEach(async(langDir) => {
//     const files = await glob(`**/*.${langDir}`, {
//       cwd: path.join(dataSetDir, langDir)
//     });
//     result.push(files)
//   });
//
//   return result
// };
//

//
// const getFilesData = async(dataSetDir) => {
//   const elements = await fs.readdir(dataSetDir);
//   const promises = elements.map(langDir => {
//     return glob(`**/*.${langDir}`, {
//       cwd: path.join(dataSetDir, langDir)
//     });
//   });
//
//   const result = await Promise.all(promises)
//
//   return [].concat(...result)
// };

// const getFilesData = async (dataSetDir) => {
//   const elements = await fs.readdir(dataSetDir);
//
//   return elements.reduce(async(resultPromise, langDir) => {
//     const files = await glob(`**/*.${langDir}`, {
//       cwd: path.join(dataSetDir, langDir)
//     });
//
//     const result = await resultPromise;
//
//     return result.concat(files.map(file =>
//       path.join(dataSetDir, langDir, file)
//     ));
//   }, []);
// };


(async () => {
  const result = await getFilesData(dirPath);
  console.log(result);
})();
