class Dough {
  constructor () {
    this.ingredients = [];
  }

  add (ingredient) {
    this.ingredients.push(ingredient)
  }

  getIngredients () {
    return this.ingredients
  }
}

const delayReturn = (what, delay) => new Promise(resolve => {
  setTimeout(() => {
    resolve(what)
  }, delay * Math.random())
});

const makeDough = () => delayReturn(new Dough(), 1000);

const makeSauce = (sauceType) => delayReturn({
  sauce: 'garlic',
  determineCheese: () => 'camembert'
}, 1000);

const makeCheese = (cheeseType) => delayReturn('camembert', 500);

/*
====    1  ====
async function makePizza(sauceType = 'red') {
  let dough = await makeDough();
  let sauce = await makeSauce(sauceType);
  let cheese = await makeCheese(sauce.determineCheese());

  dough.add(sauce);
  dough.add(cheese);

  return dough;
}

(async () => {
  const p = await makePizza();
  console.log(p.getIngredients());
})();

//
//|-------- dough --------> |-------- sauce --------> |-- cheese -->
//
*/

//|-------- dough -------->
//|-------- sauce --------> |-- cheese -->


/*
async function makePizza(sauceType = 'red') {

  let [ dough, sauce ] = await Promise.all([ makeDough(), makeSauce(sauceType) ]);
  let cheese = await makeCheese(sauce.determineCheese());

  dough.add(sauce);
  dough.add(cheese);

  return dough;
}
*/






//|-------- dough -------->
//|--- sauce --->           |-- cheese -->




/*
function makePizza(sauceType = 'red') {
  let doughPromise  = makeDough();
  let saucePromise  = makeSauce(sauceType);
  let cheesePromise = saucePromise.then(sauce =>
    makeCheese(sauce.determineCheese())
  );

  return Promise.all([ doughPromise, saucePromise, cheesePromise ])
    .then(([ dough, sauce, cheese ]) => {

      dough.add(sauce);
      dough.add(cheese);

      return dough;
    });
}
*/

/*
async function makePizza(sauceType = 'red') {

  let doughPromise = makeDough();
  let saucePromise = makeSauce(sauceType);

  let sauce  = await saucePromise;
  let cheese = await makeCheese(sauce.determineCheese());
  let dough  = await doughPromise;

  dough.add(sauce);
  dough.add(cheese);

  return dough;
}
*/


/*
async function makePizza(sauceType = 'red') {
  let prepareDough  = async () => makeDough();
  let prepareSauce  = async () => makeSauce(sauceType);
  let prepareCheese = async () =>
    makeCheese((await prepareSauce()).determineCheese());


  let [ dough, sauce, cheese ] = await Promise.all([ prepareDough(), prepareSauce(), prepareCheese()]);

  dough.add(sauce);
  dough.add(cheese);

  return dough;
}

*/