// create promise

const delayWriter = (delay, cb) => {
  setTimeout(() => {
    cb();
  }, delay)
};

const fn = () => {
  console.log('I wait to long')
};



const delayer = (delay) =>
  new Promise((resolve, reject) => {
     setTimeout(() => {
       resolve();
     }, delay)
  });


delayer(1000).then(() => {
  fn();
});
