//bad
const getUser = (userId) => Promise.resolve({
  id: 1,
  userName: 'Bob',
  company: 1
});
const getCompany = (user) => Promise.resolve({
  id: 1,
  name: 'Xsolve'
});

/*
const getUserCompany = (userId) =>
  getUser(1).then(user => {
    return getCompany(user).then(company => {
      return company.name
    })
  });
*/

/*
getUserCompany(1)
  .then(companyName => {
    console.log(companyName)
});
*/


/*
const getUserCompany2 = (userId) =>
  getUser(1).then(user =>
    getCompany(user).then(company =>
      company.name
    )
  );
*/

/*
getUserCompany2(1)
  .then(companyName => {
    console.log(companyName)
  });
*/

/*
const getUserCompany3 = (userId) =>
  getUser(1)
    .then(getCompany)
    .then(company => company.name);
*/

/*
getUserCompany3(1).then(companyName =>{
  console.log(companyName);
});
*/