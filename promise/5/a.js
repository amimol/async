//bad
const utils = require('../../utils');
const express = require('express');
const app = express();

const isAdmin = (req) => {
  const { username } = req.params;
  utils.getUser(username).then(user => {
    if (user.isAdmin && user.posts > 2) {
      return true
    } else {
      return false;
    }
  })
};


// routing
app.get('/:username', (req, res) => {
  if (isAdmin(req)) {
    res.send('HEY ADMIN')
  } else {
    res.send('Uciekaj stąd =)')
  }
});

app.listen(3000);