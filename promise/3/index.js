// chain
// Info about immediately execution

const wait = (what, delay) => new Promise(resolve => {
  setTimeout(() => {
    resolve(what);
  }, delay)
});

const say = (what) => console.log(what);

const promise1 = wait('BOO', 1000)
  .then(data => {
    return data;
});


const promise2 = wait('FOO', 2000)
  .then(data => {
    return data;
  });

Promise.race([promise2, promise1])
  .then((p1) => {
    console.log(p1);
  })






//
// const delayedSay = (what) =>  wait(() => say(what), 100* Math.random());



//
//
//
// delayedSay('Hello')
//   .then(() => {
//     return Promise.all([
//       delayedSay('1'),
//       delayedSay('2'),
//     ])
//   })
//   .then(() => {
//     delayedSay('3');
//   });
//
//
