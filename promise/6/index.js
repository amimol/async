const fs = require('fs');
const path = require('path');
const promisify = require('es6-promisify');

const filePath = path.resolve(__dirname, 'baz.txt');


/*
const getFile = (filePath) => new Promise((resolve, reject) => {
  fs.readFile(filePath, (err, data) => {
    if (err) {
      reject(err);
    } else {
      resolve(data)
    }
  })
});
*/

/*
getFile(filePath).then(data => {
  console.log(data.toString());
});
*/

/*
const getFile = promisify(fs.readFile, fs);
getFile(filePath).then(data => {
  console.log(data.toString());
});
*/