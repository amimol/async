// create promise

const delayWriter = (delay, cb) => {
  setTimeout(() => {
    cb(delay);
  }, delay)
};

const fn = (delay) => {
  console.log(`I wait ${delay} ms`)
};



delayWriter(1000, fn);
/*

const delayer = (delay) =>
  new Promise((resolve, reject) => {
     setTimeout(() => {
       resolve(delay);
     }, delay)
  });


/*
delayer(1000).then((delay) => {
  fn(delay);
});
*/
