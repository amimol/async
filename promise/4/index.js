//Write a app which say "Hello" three times each second

const deleayPromise = (delay) => new Promise(resolve => {
  setTimeout(() => {
    console.log('Hello');
    resolve();
  }, delay)
});

/*
deleayPromise().then(() => {
  deleayPromise(1000).then(() => {
    deleayPromise(1000)
  })
});
*/

/*
for (let i = 0; i < 3; i++) {
  setTimeout(() => {
    console.log(`Hello`);
  }, i * 1000);
}
*/


/*
const sayHello = (n) => {
  if (!n) { return }
  setTimeout(() => {
    console.log(`Hello`);
    sayHello(n - 1);
  }, 1000);
};
sayHello(3);
*/

/*
let n = 3;
const handler = setInterval(() => {
  console.log('Hello');
  n--;
  if (!n) {
    clearInterval(handler)
  }
}, 1000);
*/