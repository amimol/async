const express = require('express');
const utils = require('../../utils');
const app = express();

// routing
app.get('/', (req, res) => {
  res.send(
    `Hello World!
    Use /zip/:id to generate zip file!
`)
});


app.get('/zip/:zipId', (req, res) => {
  const { zipId } = req.params;
  utils.getOrGenerateZip(zipId, (err, data) => {
    res.send(`Zip data ${data}`);
  })
});

app.listen(3000);