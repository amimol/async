const express = require('express');
const utils = require('../../utils');
const app = express();

// routing
app.get('/', (req, res) => {
  res.send(
    `Hello World!
    Use /zip/:id to generate zip file!
`)
});


app.get('/zip/:zipId', (req, res) => {
  const { zipId } = req.params;
  if (utils.isZipGenerated(zipId)) {
    res.send(`Zip data ${utils.getZip(zipId)}`);
  } else {
    process.nextTick(() => {
      utils.getOrGenerateZip(zipId, () => {})
    });
    res.send('PROCESSING');
  }
});


setTimeout(() => {

}, 0)

app.listen(3000);
