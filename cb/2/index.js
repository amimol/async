const utils = require('../../utils');

const geoLocation = [{
  lat: '11.11', //Gliwice
  long: '11.11',
}, {
  lat: '22.22', //Warszawa
  long: '22.22',
}];

const result = [];

geoLocation.forEach(geoData => {
   utils.getCityName(geoData, (err, cityName) => {
     result.push(cityName)
  });
});


setTimeout(() => {
  console.log(result);
}, 2000);

