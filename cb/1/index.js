const fs = require('fs');
const path = require('path');

const dirPath = path.resolve(__dirname, 'data');


fs.readdir(dirPath, (err, files) => {
  files.forEach(file => {
    const filePath = path.resolve(dirPath, file);

    fs.readFile(filePath, (err, data) =>{
      console.log(data.toString());
    });
  });
});
