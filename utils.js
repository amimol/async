const inMemoryZipStorage = {};

const getCityName = (geoCode, cb) => {
  const delay = 100 * Math.random();
  if ('11.11' === geoCode.lat) {
    setTimeout(() => {
      cb(null, 'Gliwice')
    }, delay)
  } else if ('22.22' === geoCode.lat) {
    setTimeout(() => {
      cb(null, 'Warszawa')
    }, delay)
  } else if ('33.33' === geoCode.lat) {
    setTimeout(() => {
      cb(null, 'Sosnowiec')
    }, delay)
  } else {
    cb('Unable to fetch')
  }
};

const printData = (data, delay = 2000) => {
  setTimeout(() => {
    console.log(data);
  }, delay);
};

const compress = (id, cb) => {
  for (let i = 0; i < 999999999; i++) {}
  cb(null, `__@@ ${id} @@__`);
};

const getOrGenerateZip = (id, cb) => {
  if (inMemoryZipStorage.id) {
    cb(null, inMemoryZipStorage.id)
  } else {
    compress(id, (err, result) => {
      inMemoryZipStorage.id = result;
      cb(null, result)
    })
  }
};

const isZipGenerated = (id) =>
  !!inMemoryZipStorage.id;

const getZip = (id) =>
  inMemoryZipStorage.id;

const getUser = (userName) => Promise.resolve({
  userName: userName,
  isAdmin: 'admin' === userName,
  posts: Math.floor(10 * Math.random())
});

module.exports = {
  getCityName,
  printData,
  compress,
  getOrGenerateZip,
  isZipGenerated,
  getZip,
  getUser
};









