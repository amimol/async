const delayReturn = (what, delay) => new Promise(resolve => {
  setTimeout(() => {
    resolve(what)
  }, delay)
});

const a = delayReturn('4', 40);
const b = delayReturn('20', 2);

const all = (promises = [] ) => new Promise(resolve => {
  const returnValues = [];

  promises.map((promise, i) => {
    promise.then(result => {

      returnValues[i] = result;

      if (returnValues.filter(v => v!== undefined).length === promises.length) {
        resolve(returnValues);
      }
    })
  })
});

all([a,b]).then(vals => {
   console.log(vals);
});