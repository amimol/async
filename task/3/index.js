const delayReturn = (what, delay) => new Promise(resolve => {
  console.log('STARTING');
  setTimeout(() => {
    resolve(what)
  }, delay)
});

const createA = () => delayReturn('4', 4000);
const createB = () => delayReturn('20', 2000);

/*
const waterfall = (promisesFactory = []) => {
  let promiseChain = Promise.resolve();
  promisesFactory.forEach(promiseFactory => {
    promiseChain = promiseChain
      .then(() => promiseFactory())
  });

  return promiseChain
};
*/

const waterfall = async(promisesFactory = []) => {
  // for (let item of promisesFactory) {
  //    await item(); //strange bind of your code.
  // }

  // for (let i = 0 ; i < promisesFactory.length; i++) {
  //   await promisesFactory[i]();
  // }


  // promisesFactory.forEach(async(promiseFactory) => {
  //   const r = await promiseFactory()
  // });

  return Promise.resolve();
};










waterfall([createA,createB]).then(() => {
  console.log('allDone');
});