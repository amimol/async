const delayReturn = (what, delay) => new Promise(resolve => {
  setTimeout(() => {
    resolve(what)
  }, delay)
});

const a = delayReturn('4', 40);
const b = delayReturn('20', 2);

const race = (promises = []) => new Promise(resolve => {
  promises.forEach(p => {
    p.then(data => {
      resolve(data)
    })
  })
});

race([a,b]).then(data => {
  console.log(data)
});